export const environment = {
    "envId": "0dfda63e-f405-6b19-dd2c-af81e7197309",
    "name": "dev",
    "properties": {
        "production": false,
        "baseUrl": "http://localhost:3000/bhive-art/",
        "tenantName": "neutrinos-delivery",
        "appName": "Chatbot",
        "namespace": "com.neutrinos-delivery.Chatbot",
        "isNotificationEnabled": false,
        "googleMapKey": "AIzaSyCSTnVwijjv0CFRA4MEeS-H6PAQc87LEoU",
        "firebaseSenderId": "FIREBASE_SENDER_ID",
        "firebaseAuthKey": "FIREBASE_AUTH_KEY",
        "authDomain": "FIREBASE_AUTH_DOMAIN",
        "databaseURL": "FIREBASE_DATABASE_URL",
        "storageBucket": "FIREBASE_STORAGE_BUCKET",
        "appDataSource": "neutrinos-delivery-rt",
        "appAuthenticationStrategy": "basicAuth",
        "basicAuthUser": "username",
        "basicAuthPassword": "password",
        "useDefaultExceptionUI": true
    }
}